## Introduction
이 프로젝트의 이름은 Neural Network-based Fault Localization using Code and Change Metrics 이다.

이 프로젝트의 주요 아이디어는 신경망(Neural Network)를 기반으로하는 결함 위치 추정(Fault localization)에 코드 변경 지표(Code Change Metrics)를 추가하여 성능을 향상시킨 프로젝트이다.

프로젝트의 타겟 프로그램으로는 Defectsj4의 Chart Time Lang Math Closure 사용한다.

코드 변경 지표(Code Change Metrics)는 FLUCCS의 데이터에서 제공하는 Age, Churn을 사용한다.

## Architecture
#### Overall Architecture
To be determined

## Requirements

#### 1. Defectsj4 Github의 Requirements를 참고하여 설치 

https://github.com/rjust/defects4j 

#### 2. FLUCCS Bitbucket의 Dependencies and Requirements를 참고하여 설치

https://bitbucket.org/teamcoinse/fluccs

#### 3. NNFL using code and Change Metrics 사용을 위한 추가 설치

```shell
# Perl Install Package
sudo cpan Try::Tiny Switch Cwd DBI File::Slurp JSON

# Python3 Install Package
pip3 install pandas numpy matplotlib tqdm
pip3 install tensorflow tensorboard
(If you have a GPU), pip3 install tensorflow-gpu tensorboard
```

## Execution Steps

- 관련 사이트 FLUCCS Bitbucket : https://bitbucket.org/teamcoinse/fluccs

1) FLUCCS Bitbucket의 Getting started 단계까지 실행

2) perl_files/requirement 폴더에서 extraing_lines.pm, 파일을 fluccs/perl/에 덮어쓰기, gen_coverage_rel_info.pl 파일을 fluccs/coverage/에 각각 덮어쓰기

3) FLUCCS Bitbucket의 Preparing the Dataset의 3. 단계까지 실행

4) perl_files/executor 폴더에서 checkout과 prepare한 프로젝트와 같은 이름의 perl 명령어를 실행

```shell
# How to execute perl 
perl gen_chart.pl
```

5) FLUCCS Bitbucket의 Preparing the Dataset의 5. 단계까지 실행

6) python_files의 모든 python 파일을 working directory가 존재하는 폴더에 복사

7) main.py 실행


## main.py - Menu Description

#### 1. FLUCCS을 이용하여 생성한 coverage & test result 정보를 가지고있는 JSON 파일을 병합(merge)하는 과정

| Generate coverage data    | Description | Abbreviation |
|:--------------------------|:-----------|:-------------:|
| gen_coverage_data         | 메소드의 커버리지 및 테스트 결과만 포함한 데이터 생성| Origin |
| gen_coverage_ccm_data     | 메소드의 커버리지 및 테스트 결과 데이터 + 코드 변경 지표 Age, Churn 추가 | Average |
| gen_coverage_ccm_data_min | 메소드의 커버리지 및 테스트 결과 데이터 + 코드 변경 지표 Age, Churn 추가 | Min | 
| gen_coverage_ccm_data_max | 메소드의 커버리지 및 테스트 결과 데이터 + 코드 변경 지표 Age, Churn 추가 | Max |

```shell
### gen_coverage_data
# Option p is a Project Name[Chart|Time|Lang|Math|Closure|Mockito]
# Option v is a Project Bug Version 
# Option w is a working Directory 

python3 main.py gen_coverage_data -p Chart -v 1 -w Chart_1b

### gen_coverage_ccm_data, gen_coverage_ccm_data_min, gen_coverage_ccm_data_max
# ccm의 경우 gen_coverage_data로 생성된 데이터를 이용하기에 -v, -w option을 자동으로 실행

python3 main.py gen_coverage_ccm_data -p Chart

python3 main.py gen_coverage_ccm_data_min -p Chart

python3 main.py gen_coverage_ccm_data_max -p Chart
```

- Input : 각 테스트 케이스에 대한 커버리지 정보 및 테스트 결과 JSON 파일 
- Output : 모든 테스트 커버리지 및 테스트 결과에 대한 것을 병합하여, Pass / Fail 한 npy(Numpy Array) 데이터 2개로 만듬

```
Pass / Fail 2개로 나누는 이유 : 
Defects4j의 Pass, Fail 테스트 비율이 비대칭(Fail이 적음), Sampling시 Fail 테스트 케이스에 가중치를 주기위해 2개로 나누었음.
```

#### 2. 사용자가 원하는 sampling 크기로 훈련 데이터를 생성하는 과정

| Generate train data    | Description | Abbreviation |
|:---------------------- |:------------|:------------:|
| gen_train_data         | 메소드의 커버리지 및 테스트 결과만 포함한 데이터를 훈련 데이터로 변환 | Origin |
| gen_train_ccm_data     | 메소드의 커버리지 및 테스트 결과 + 코드 변경 지표 Age, Churn 포함한 데이터를 훈련 데이터로 변환 | Average |
| gen_train_ccm_data_min | 메소드의 커버리지 및 테스트 결과만 포함한 데이터를 훈련 데이터로 변환 | Min |
| gen_train_ccm_data_max | 메소드의 커버리지 및 테스트 결과만 포함한 데이터를 훈련 데이터로 변환 | Max |

```shell
### gen_train_data, gen_train_ccm_data, gen_train_ccm_data_min, gen_train_ccm_data_max
# Option p is a Project Name[Chart|Time|Lang|Math|Closure|Mockito]
# Option v is a Project Bug Version 
# Option s is a Sampling Number

python3 main.py gen_train_data -p Chart -v 1 -s 1000

python3 main.py gen_train_ccm_data -p Chart -v 1 -s 1000

python3 main.py gen_train_ccm_data_min -p Chart -v 1 -s 1000

python3 main.py gen_train_ccm_data_max -p Chart -v 1 -s 1000
```

- Input  : Pass, Fail NPY 데이터
- Output : 1개의 NPY 데이터 

```
Sampling을 하는 이유 :
모든 테스트 케이스를 훈련데이터로 사용시 훈련시간이 오래걸려 효율성이 떨어짐. 
sampling을 1000, 2000, 3000, 4000개 진행시 2000이상 부터는 별다른 차이가 없음. 
대부분의 프로젝트에서 500, 1000, 1500 정도로 sampling하여 훈련한 것이 좋은 성능을 보임. 
sampling을 1000, 1500으로 진행할 것을 추천
```

#### 3. 생성된 npy 파일을 tensorflow을 이용해 훈련하는 과정

| Train data         | Description | Abbreviation |
|:-------------------|:-----------|:-------------:|
| train_data         | Origin 데이터(Age, Churn 제외)를 훈련 | Origin | 
| train_ccm_data     | Average 데이터(Age, Churn 데이터 포함)을 훈련 | Average | 
| train_ccm_data_min | Min 데이터(Age, Churn 데이터 포함)을 훈련 | Min |
| train_ccm_data_max | Max 데이터(Age, Churn 데이터 포함)을 훈련 | Max |

```shell
### train_data, train_ccm_data, train_ccm_data_min, train_ccm_data_max
# Option p is a Project Name[Chart|Time|Lang|Math|Closure|Mockito]
# Option v is a Project Bug Version 
# Option s is a Sampling Number
# Option n is a Node Number

python3 main.py train_data -p Chart -v 1 -s 1000 -n 1500

python3 main.py train_ccm_data -p Chart -v 1 -s 1000 -n 1500

python3 main.py train_ccm_data_min -p Chart -v 1 -s 1000 -n 1500

python3 main.py train_ccm_data_max -p Chart -v 1 -s 1000 -n 1500
```


- Input : NPY Data
- Output : 각 메소드의 fault localization score 값, csv 파일로 저장 

#### 4. Defectsj4 & FLUCCS에서 Fault의 정보를 가져오는 과정

| Generate fault data | Description | 
|:--------------------|:------------|
| gen_fault_data | Defects4j의 결함을 가지는 Method의 정보를 가져와 CSV 파일로 저장 |

```shell
### gen_fault_data
# Option p is a Project Name[Chart|Time|Lang|Math|Closure|Mockito]

python3 main.py gen_fault_data -p Chart -s 1000 -e 300 -n 1500
```

- Output : Real Fault Method 정보 데이터 (.csv)

#### 5. 3.에서 결과로 출력된 suspiciousness score 데이터 + 4.에서 Real Fault 정보를 가져와 병합하는 과정

| Generate result data | Description | 
|:-------------------- |:------------|
| gen_result_data      | 4번의 Origin에서 훈련하여 나온 Suspiciousness score와 Real Fault Method를 비교하여 그래프로 출력 |  
| gen_result_ccm_data  | 4번의 Average에서 훈련하여 나온 Suspiciousness score와 Real Fault Method를 비교하여 그래프로 출력 | 
| gen_result_ccm_min   | 4번의 Min에서 훈련하여 나온 Suspiciousness score와 Real Fault Method를 비교하여 그래프로 출력 |  
| gen_result_ccm_max   | 4번의 Max에서 훈련하여 나온 Suspiciousness score와 Real Fault Method를 비교하여 그래프로 출력 |

```shell
### gen_result_data, gen_result_ccm_data, gen_result_ccm_data_min, gen_result_ccm_data_max
# Option p is a Project Name[Chart|Time|Lang|Math|Closure|Mockito]
# Option s is a Sampling Number
# Option e is a Epoch Number
# Option n is a Node Number

python3 main.py gen_result_data -p Chart -s 1000 -e 300 -n 1500

python3 main.py gen_result_ccm_data -p Chart -s 1000 -e 300 -n 1500

python3 main.py gen_result_ccm_data_min -p Chart -s 1000 -e 300 -n 1500

python3 main.py gen_result_ccm_data_max -p Chart -s 1000 -e 300 -n 1500
```

## Python Files information 
| Python File Name | Description |
|:-----------------|:-----------:|
| configurator.py  | main.py에서 입력으로 들어온 args를 처리하는 모듈 |
| generator.py     | 훈련을 위한 데이터를 생성(npy data)하는 모듈 |
| trainer.py       | 데이터를 훈련하여 Suspiciousness score을 추출하는 모듈 | 
| concatenator.py  | 결과 데이터를 합치는 모듈 |
| drawer.py        | 결과 데이터를 시각적으로 표현하는 모듈 |
| executor.py      | 명령어를 다중으로 처리하는 모듈 |
| main.py          | 실행하고자 하는 메뉴를 선택하는 모듈 |


## Perl Files information 
| Perl File Name           | Description |
|:-------------------------|:-----------:|
| extracting_lines.pm      | NNFL using CCM에서 사용하기 위해 추가적인 코드 추가 |
| gen_coverage_rel_info.pl | NNFL using CCM에서 사용하기 위해 추가적인 코드 추가 |

## Reference Sites & Papers
Defectsj4 github : https://github.com/rjust/defects4j

FLUCCS bitbucket : https://bitbucket.org/teamcoinse/fluccs

Wong, W. Eric, and Yu Qi - BP neural network-based effective fault localization

Sohn, Jeongju, and Shin Yoo - FLUCCS: using code and change metrics to improve fault localization



