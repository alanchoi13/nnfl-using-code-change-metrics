use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper qw(Dumper);
use JSON;
use File::Slurp qw(read_file write_file);
use YAML::XS qw(Load Dump);



my $environment_d4j = $ENV{"D4J_HOME"};
my $lang_directory = $environment_d4j.'/framework/bin/fluccs/method_stmt/Lang';
my @lang_csv_files = glob($lang_directory.'/stmt_mth_id_Lang_*.csv');
mkdir($lang_directory."/stmt_info");

foreach my $file (@lang_csv_files){
    my %lang_files_information;
    my $column_value1;
    my $column_value2;
    my $column_value3;
    my $file_name = basename($file);

    $file_name =~ s/csv//g;

    open (FH1, "<" , $file) or die ("Could not open $file!");
    while (my $line = <FH1>)
    {
        ($column_value1,$column_value2,$column_value3) = split (',', $line);
        $lang_files_information{$column_value1}{$column_value2} = '0';
    }
    close(FH1);
    $lang_files_information{"testcaseName"} = '';


    open (FH2, ">", $lang_directory."/stmt_info/".basename($file_name)."json") or die ("Could not open $file!");
    print FH2 encode_json(\%lang_files_information);
    close(FH2);

}

