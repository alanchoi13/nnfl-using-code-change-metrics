import numpy as np
import tensorflow as tf
import pandas as pd
import timeit
import os, sys
import shutil
from configurator import Configurator


class Trainer:
    def __init__(self, project_name=None, faulty_version=None, work_dir=None, sample_num=None, node_number=None):
        self.d4j_env = os.environ["D4J_HOME"]
        self.pwd = os.getcwd()

        #self.d4j_env = "/mnt/d/workdir/defects4j"
        #self.pwd = "/mnt/c/Data/Chart"

        self.project_name = project_name
        self.faulty_version = faulty_version
        self.working_directory = work_dir
        self.data_category = "/method_data/"
        self.sample_number = sample_num
        self.node_number = node_number
        self.config = tf.ConfigProto(device_count={'GPU': 0})

        self.train_data_origin_path = "train_data/origin_data/"
        self.train_data_ccm_path = "train_data/ccm_data/"
        self.train_data_ccm_x_path = "train_data/ccm_x_data/"
        self.train_result_data_origin_path = "train_result_data/origin_data/"
        self.train_result_data_ccm_path = "train_result_data/ccm_data/"
        self.base_path = self.pwd + "/outputs/" + self.project_name + "/"

    def train_coverage_data(self):
        if os.path.exists(self.base_path + self.train_result_data_origin_path+"logs/" + self.project_name + "_" + self.faulty_version):
            shutil.rmtree(self.base_path + self.train_result_data_origin_path+"logs/" + self.project_name + "_" + self.faulty_version,
                          ignore_errors=True)
        if not os.path.exists(
                self.base_path + self.train_result_data_origin_path+"logs/" + self.project_name + "_" + self.faulty_version):
            os.makedirs(self.base_path + self.train_result_data_origin_path+"logs/" + self.project_name + "_" + self.faulty_version)
        tf.reset_default_graph()
        if not os.path.exists(
                self.base_path + self.train_result_data_origin_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version)):
            os.makedirs(
                self.base_path + self.train_result_data_origin_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version))

        if not os.path.exists(
                self.base_path + self.train_result_data_origin_path + "train_result/" + self.project_name + "_" + self.faulty_version):
            os.makedirs(
                self.base_path + self.train_result_data_origin_path + "train_result/" + self.project_name + "_" + self.faulty_version)

        print("============= START TRAIN DATA " + str(self.faulty_version) + "=============")

        xy = np.load(
            self.base_path + self.train_data_origin_path + "train_" + self.project_name + "_" + str(
                self.faulty_version) + "_data_" + str(self.sample_number) + ".npy")

        x_data = xy[:, 0:-1]
        y_data = xy[:, [-1]]

        test_x = np.eye(x_data.shape[1])
        start = timeit.default_timer()
        y_int_data = y_data.astype(int)

        X = tf.placeholder(tf.float32, name='features')
        Y = tf.placeholder(tf.float32, name='labels')

        pre_Y = tf.placeholder(tf.float32)

        W1 = tf.Variable(tf.random_uniform([x_data.shape[1], int(self.node_number)], -1., 1.), name='W1')
        b1 = tf.Variable(tf.zeros([int(self.node_number)]))
        L1 = tf.add(tf.matmul(X, W1), b1)
        L1 = tf.nn.relu(L1)
        #
        # W2 = tf.Variable(tf.random_uniform([512, 256], -1., 1.), name='W2')
        # b2 = tf.Variable(tf.zeros([256]))
        # L2 = tf.add(tf.matmul(L1, W2), b2)
        # L2 = tf.nn.relu(L2)

        # W3 = tf.Variable(tf.random_uniform([256, 128], -1., 1.), name="W3")
        # b3 = tf.Variable(tf.zeros([128]))
        # L3 = tf.add(tf.matmul(L2, W3), b3)
        # L3 = tf.nn.relu(L3)

        W4 = tf.Variable(tf.random_uniform([int(self.node_number), 1], -1., 1.), name="W3")
        b4 = tf.Variable(tf.zeros([1]))

        model = tf.add(tf.matmul(L1, W4), b4)

        global_step = tf.Variable(0, trainable=False, name='global_step')
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y, logits=model))
        v_loss = tf.summary.scalar('loss_lang_' + str(self.faulty_version), loss)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=global_step)

        _, acc_op = tf.metrics.accuracy(labels=y_int_data, predictions=pre_Y)
        _, rec_op = tf.metrics.recall(labels=y_int_data, predictions=pre_Y)
        _, pre_op = tf.metrics.precision(labels=y_int_data, predictions=pre_Y)

        f1 = 2 * pre_op * rec_op / (pre_op + rec_op)

        v_acc = tf.summary.scalar('accuracy_lang_' + str(self.faulty_version), acc_op)
        v_rec = tf.summary.scalar('recall_lang_' + str(self.faulty_version), rec_op)
        v_pre = tf.summary.scalar('precision_lang_' + str(self.faulty_version), pre_op)
        v_f1 = tf.summary.scalar('f1 score_lang_' + str(self.faulty_version), f1)

        merged = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        init_l = tf.local_variables_initializer()

        with tf.Session(config=self.config) as sess:
            sess.run(init)
            sess.run(init_l)
            writer = tf.summary.FileWriter(
                self.base_path + self.train_result_data_origin_path + "logs/" + self.project_name + "_" + self.faulty_version + "/",
                sess.graph)
            saver = tf.train.Saver()

            for step in range(300):
                _, loss_val, pred_val = sess.run([train_op, loss, tf.nn.sigmoid(model)],
                                                 feed_dict={X: x_data, Y: y_data})
                pred_th = np.where(pred_val > 0.5, 1, 0)
                acc, rec, pre, merged_list = sess.run([acc_op, rec_op, pre_op, merged],
                                                      feed_dict={X: x_data, Y: y_data, pre_Y: pred_th})
                f1_v = 2 * pre * rec / (pre + rec)

                writer.add_summary(merged_list, step + 1)
                if (step + 1) % 50 == 0:
                    print("currnet step : ", step + 1)
                    print("accuracy :", acc)
                    print("recall :", rec)
                    print("precision :", pre)
                    print("f1 score : ", f1_v)
                    print("loss :", loss_val)

                    # ckpt_path = saver.save(sess,
                    #                        basic_path + "training_data/train_ckpt/" + self.project_name + "_" + str(self.faulty_version) + "/" + self.project_name + "_" + str(self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(step + 1) + "_nodes-" + self.node_number,
                    #                        step + 1)
                    # print("===== save ckpt file:", ckpt_path)

                    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
                    df = pd.DataFrame(prediction)
                    df.to_csv(
                        self.base_path + self.train_result_data_origin_path + "train_result/" + self.project_name + "_" + str(
                            self.faulty_version) + "/" + self.project_name + "_" + str(
                            self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(
                            step + 1) + "_nodes-" + self.node_number + ".csv", header=False)

            end = timeit.default_timer()
            print("Run Time", end - start)
            writer.flush()

    def train_coverage_ccm_data(self):
        if os.path.exists(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            shutil.rmtree(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version,
                          ignore_errors=True)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            os.makedirs(self.base_path + self.train_result_data_ccm_path +"logs/" + self.project_name + "_" + self.faulty_version)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + self.faulty_version):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path +"train_result/" +self.project_name + "_" + self.faulty_version)
        tf.reset_default_graph()
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version)):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version))

        print("============= START TRAIN DATA " + str(self.faulty_version) + "=============")

        xy = np.load(
            self.base_path + self.train_data_ccm_path + "train_" + self.project_name + "_" + str(
                self.faulty_version) + "_data_" + str(self.sample_number) + ".npy")
        # xy = np.nan_to_num(xy)
        # xy[~np.isnan(xy).any(axis=1)]
        # xy[np.isnan(xy)] = 0.5
        # print(xy)

        x_data = xy[:, 0:-1]
        y_data = xy[:, [-1]]

        test_x = np.load(self.base_path + self.train_data_ccm_x_path + "test_" + self.project_name + "_" + str(
            self.faulty_version) + "_data.npy")
        # test_x = np.nan_to_num(test_x)
        # test_x[np.isnan(test_x)] = 0.5
        # print(test_x)
        # print(test_x)
        start = timeit.default_timer()
        y_int_data = y_data.astype(int)

        X = tf.placeholder(tf.float32, name='features')
        Y = tf.placeholder(tf.float32, name='labels')

        pre_Y = tf.placeholder(tf.float32)

        W1 = tf.Variable(tf.random_uniform([x_data.shape[1], int(self.node_number)], -1., 1.), name='W1')
        b1 = tf.Variable(tf.zeros([int(self.node_number)]))
        L1 = tf.add(tf.matmul(X, W1), b1)
        L1 = tf.nn.relu(L1)
        #
        # W2 = tf.Variable(tf.random_uniform([512, 256], -1., 1.), name='W2')
        # b2 = tf.Variable(tf.zeros([256]))
        # L2 = tf.add(tf.matmul(L1, W2), b2)
        # L2 = tf.nn.relu(L2)

        # W3 = tf.Variable(tf.random_uniform([256, 128], -1., 1.), name="W3")
        # b3 = tf.Variable(tf.zeros([128]))
        # L3 = tf.add(tf.matmul(L2, W3), b3)
        # L3 = tf.nn.relu(L3)

        W4 = tf.Variable(tf.random_uniform([int(self.node_number), 1], -1., 1.), name="W3")
        b4 = tf.Variable(tf.zeros([1]))

        model = tf.add(tf.matmul(L1, W4), b4)

        global_step = tf.Variable(0, trainable=False, name='global_step')

        # loss = tf.reduce_mean(-tf.reduce_sum(model * tf.log(tf.nn.softmax(Y + 1e-50)), [1]))
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y, logits=model))
        # loss = tf.reduce_mean(total_loss(labels=Y, logits=model))
        # v_loss = tf.summary.scalar('loss_lang_' + str(self.faulty_version), loss)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=global_step)

        _, acc_op = tf.metrics.accuracy(labels=y_int_data, predictions=pre_Y)
        _, rec_op = tf.metrics.recall(labels=y_int_data, predictions=pre_Y)
        _, pre_op = tf.metrics.precision(labels=y_int_data, predictions=pre_Y)

        f1 = 2 * pre_op * rec_op / (pre_op + rec_op)

        v_acc = tf.summary.scalar('accuracy_lang_' + str(self.faulty_version), acc_op)
        v_rec = tf.summary.scalar('recall_lang_' + str(self.faulty_version), rec_op)
        v_pre = tf.summary.scalar('precision_lang_' + str(self.faulty_version), pre_op)
        v_f1 = tf.summary.scalar('f1 score_lang_' + str(self.faulty_version), f1)

        merged = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        init_l = tf.local_variables_initializer()

        with tf.Session(config=self.config) as sess:
            sess.run(init)
            sess.run(init_l)
            writer = tf.summary.FileWriter(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version + "/",
                sess.graph)
            saver = tf.train.Saver()

            for step in range(300):
                _, loss_val, pred_val = sess.run([train_op, loss, tf.nn.sigmoid(model)],
                                                 feed_dict={X: x_data, Y: y_data})

                # print(pred_val)
                pred_th = np.where(pred_val > 0.5, 1, 0)
                acc, rec, pre, merged_list = sess.run([acc_op, rec_op, pre_op, merged],
                                                      feed_dict={X: x_data, Y: y_data, pre_Y: pred_th})
                f1_v = 2 * pre * rec / (pre + rec)

                writer.add_summary(merged_list, step + 1)
                if (step + 1) % 50 == 0:
                    print("currnet step : ", step + 1)
                    print("accuracy :", acc)
                    print("recall :", rec)
                    print("precision :", pre)
                    print("f1 score : ", f1_v)
                    print("loss :", loss_val)
                    # ckpt_path = saver.save(sess,
                    #                        basic_path + "metrics_training_data/train_ckpt/" + self.project_name + "_" + str(self.faulty_version) + "/" + self.project_name + "_" + str(self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(step + 1) + "_nodes-" + self.node_number,
                    #                        step + 1)
                    # print("===== save ckpt file:", ckpt_path)
                    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
                    df = pd.DataFrame(prediction)
                    df.to_csv(self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + str(
                        self.faulty_version) + "/" + self.project_name + "_" + str(
                        self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(
                        step + 1) + "_nodes-" + self.node_number + ".csv", header=False)

            end = timeit.default_timer()
            print("Run Time", end - start)
            writer.flush()

    def train_coverage_ccm_data_min(self):
        if os.path.exists(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            shutil.rmtree(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version,
                          ignore_errors=True)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            os.makedirs(self.base_path + self.train_result_data_ccm_path +"logs/" + self.project_name + "_" + self.faulty_version)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + self.faulty_version):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path +"train_result/" +self.project_name + "_" + self.faulty_version)
        tf.reset_default_graph()
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version)):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version))

        print("============= START TRAIN DATA " + str(self.faulty_version) + "=============")

        xy = np.load(
            self.base_path + self.train_data_ccm_path + "min_train_" + self.project_name + "_" + str(
                self.faulty_version) + "_data_" + str(self.sample_number) + ".npy")
        # xy = np.nan_to_num(xy)
        # xy[~np.isnan(xy).any(axis=1)]
        # xy[np.isnan(xy)] = 0.5
        # print(xy)

        x_data = xy[:, 0:-1]
        y_data = xy[:, [-1]]

        test_x = np.load(self.base_path + self.train_data_ccm_x_path + "min_test_" + self.project_name + "_" + str(
            self.faulty_version) + "_data.npy")
        # test_x = np.nan_to_num(test_x)
        # test_x[np.isnan(test_x)] = 0.5
        # print(test_x)
        # print(test_x)
        start = timeit.default_timer()
        y_int_data = y_data.astype(int)

        X = tf.placeholder(tf.float32, name='features')
        Y = tf.placeholder(tf.float32, name='labels')

        pre_Y = tf.placeholder(tf.float32)

        W1 = tf.Variable(tf.random_uniform([x_data.shape[1], int(self.node_number)], -1., 1.), name='W1')
        b1 = tf.Variable(tf.zeros([int(self.node_number)]))
        L1 = tf.add(tf.matmul(X, W1), b1)
        L1 = tf.nn.relu(L1)
        #
        # W2 = tf.Variable(tf.random_uniform([512, 256], -1., 1.), name='W2')
        # b2 = tf.Variable(tf.zeros([256]))
        # L2 = tf.add(tf.matmul(L1, W2), b2)
        # L2 = tf.nn.relu(L2)

        # W3 = tf.Variable(tf.random_uniform([256, 128], -1., 1.), name="W3")
        # b3 = tf.Variable(tf.zeros([128]))
        # L3 = tf.add(tf.matmul(L2, W3), b3)
        # L3 = tf.nn.relu(L3)

        W4 = tf.Variable(tf.random_uniform([int(self.node_number), 1], -1., 1.), name="W3")
        b4 = tf.Variable(tf.zeros([1]))

        model = tf.add(tf.matmul(L1, W4), b4)

        global_step = tf.Variable(0, trainable=False, name='global_step')

        # loss = tf.reduce_mean(-tf.reduce_sum(model * tf.log(tf.nn.softmax(Y + 1e-50)), [1]))
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y, logits=model))
        # loss = tf.reduce_mean(total_loss(labels=Y, logits=model))
        # v_loss = tf.summary.scalar('loss_lang_' + str(self.faulty_version), loss)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=global_step)

        _, acc_op = tf.metrics.accuracy(labels=y_int_data, predictions=pre_Y)
        _, rec_op = tf.metrics.recall(labels=y_int_data, predictions=pre_Y)
        _, pre_op = tf.metrics.precision(labels=y_int_data, predictions=pre_Y)

        f1 = 2 * pre_op * rec_op / (pre_op + rec_op)

        v_acc = tf.summary.scalar('accuracy_lang_' + str(self.faulty_version), acc_op)
        v_rec = tf.summary.scalar('recall_lang_' + str(self.faulty_version), rec_op)
        v_pre = tf.summary.scalar('precision_lang_' + str(self.faulty_version), pre_op)
        v_f1 = tf.summary.scalar('f1 score_lang_' + str(self.faulty_version), f1)

        merged = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        init_l = tf.local_variables_initializer()

        with tf.Session(config=self.config) as sess:
            sess.run(init)
            sess.run(init_l)
            writer = tf.summary.FileWriter(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version + "/",
                sess.graph)
            saver = tf.train.Saver()

            for step in range(300):
                _, loss_val, pred_val = sess.run([train_op, loss, tf.nn.sigmoid(model)],
                                                 feed_dict={X: x_data, Y: y_data})

                # print(pred_val)
                pred_th = np.where(pred_val > 0.5, 1, 0)
                acc, rec, pre, merged_list = sess.run([acc_op, rec_op, pre_op, merged],
                                                      feed_dict={X: x_data, Y: y_data, pre_Y: pred_th})
                f1_v = 2 * pre * rec / (pre + rec)

                writer.add_summary(merged_list, step + 1)
                if (step + 1) % 50 == 0:
                    print("currnet step : ", step + 1)
                    print("accuracy :", acc)
                    print("recall :", rec)
                    print("precision :", pre)
                    print("f1 score : ", f1_v)
                    print("loss :", loss_val)
                    # ckpt_path = saver.save(sess,
                    #                        basic_path + "metrics_training_data/train_ckpt/" + self.project_name + "_" + str(self.faulty_version) + "/" + self.project_name + "_" + str(self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(step + 1) + "_nodes-" + self.node_number,
                    #                        step + 1)
                    # print("===== save ckpt file:", ckpt_path)
                    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
                    df = pd.DataFrame(prediction)
                    df.to_csv(self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + str(
                        self.faulty_version) + "/min_" + self.project_name + "_" + str(
                        self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(
                        step + 1) + "_nodes-" + self.node_number + ".csv", header=False)

            end = timeit.default_timer()
            print("Run Time", end - start)
            writer.flush()

    def train_coverage_ccm_data_max(self):
        if os.path.exists(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            shutil.rmtree(self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version,
                          ignore_errors=True)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version):
            os.makedirs(self.base_path + self.train_result_data_ccm_path +"logs/" + self.project_name + "_" + self.faulty_version)
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + self.faulty_version):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path +"train_result/" +self.project_name + "_" + self.faulty_version)
        tf.reset_default_graph()
        if not os.path.exists(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version)):
            os.makedirs(
                self.base_path + self.train_result_data_ccm_path+"train_ckpt/" + self.project_name + "_" + str(self.faulty_version))

        print("============= START TRAIN DATA " + str(self.faulty_version) + "=============")

        xy = np.load(
            self.base_path + self.train_data_ccm_path + "train_" + self.project_name + "_" + str(
                self.faulty_version) + "_data_" + str(self.sample_number) + ".npy")
        # xy = np.nan_to_num(xy)
        # xy[~np.isnan(xy).any(axis=1)]
        # xy[np.isnan(xy)] = 0.5
        # print(xy)

        x_data = xy[:, 0:-1]
        y_data = xy[:, [-1]]

        test_x = np.load(self.base_path + self.train_data_ccm_x_path + "max_test_" + self.project_name + "_" + str(
            self.faulty_version) + "_data.npy")
        # test_x = np.nan_to_num(test_x)
        # test_x[np.isnan(test_x)] = 0.5
        # print(test_x)
        # print(test_x)
        start = timeit.default_timer()
        y_int_data = y_data.astype(int)

        X = tf.placeholder(tf.float32, name='features')
        Y = tf.placeholder(tf.float32, name='labels')

        pre_Y = tf.placeholder(tf.float32)

        W1 = tf.Variable(tf.random_uniform([x_data.shape[1], int(self.node_number)], -1., 1.), name='W1')
        b1 = tf.Variable(tf.zeros([int(self.node_number)]))
        L1 = tf.add(tf.matmul(X, W1), b1)
        L1 = tf.nn.relu(L1)
        #
        # W2 = tf.Variable(tf.random_uniform([512, 256], -1., 1.), name='W2')
        # b2 = tf.Variable(tf.zeros([256]))
        # L2 = tf.add(tf.matmul(L1, W2), b2)
        # L2 = tf.nn.relu(L2)

        # W3 = tf.Variable(tf.random_uniform([256, 128], -1., 1.), name="W3")
        # b3 = tf.Variable(tf.zeros([128]))
        # L3 = tf.add(tf.matmul(L2, W3), b3)
        # L3 = tf.nn.relu(L3)

        W4 = tf.Variable(tf.random_uniform([int(self.node_number), 1], -1., 1.), name="W3")
        b4 = tf.Variable(tf.zeros([1]))

        model = tf.add(tf.matmul(L1, W4), b4)

        global_step = tf.Variable(0, trainable=False, name='global_step')

        # loss = tf.reduce_mean(-tf.reduce_sum(model * tf.log(tf.nn.softmax(Y + 1e-50)), [1]))
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y, logits=model))
        # loss = tf.reduce_mean(total_loss(labels=Y, logits=model))
        # v_loss = tf.summary.scalar('loss_lang_' + str(self.faulty_version), loss)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=global_step)

        _, acc_op = tf.metrics.accuracy(labels=y_int_data, predictions=pre_Y)
        _, rec_op = tf.metrics.recall(labels=y_int_data, predictions=pre_Y)
        _, pre_op = tf.metrics.precision(labels=y_int_data, predictions=pre_Y)

        f1 = 2 * pre_op * rec_op / (pre_op + rec_op)

        v_acc = tf.summary.scalar('accuracy_lang_' + str(self.faulty_version), acc_op)
        v_rec = tf.summary.scalar('recall_lang_' + str(self.faulty_version), rec_op)
        v_pre = tf.summary.scalar('precision_lang_' + str(self.faulty_version), pre_op)
        v_f1 = tf.summary.scalar('f1 score_lang_' + str(self.faulty_version), f1)

        merged = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        init_l = tf.local_variables_initializer()

        with tf.Session(config=self.config) as sess:
            sess.run(init)
            sess.run(init_l)
            writer = tf.summary.FileWriter(
                self.base_path + self.train_result_data_ccm_path + "logs/" + self.project_name + "_" + self.faulty_version + "/",
                sess.graph)
            saver = tf.train.Saver()

            for step in range(300):
                _, loss_val, pred_val = sess.run([train_op, loss, tf.nn.sigmoid(model)],
                                                 feed_dict={X: x_data, Y: y_data})

                # print(pred_val)
                pred_th = np.where(pred_val > 0.5, 1, 0)
                acc, rec, pre, merged_list = sess.run([acc_op, rec_op, pre_op, merged],
                                                      feed_dict={X: x_data, Y: y_data, pre_Y: pred_th})
                f1_v = 2 * pre * rec / (pre + rec)

                writer.add_summary(merged_list, step + 1)
                if (step + 1) % 50 == 0:
                    print("currnet step : ", step + 1)
                    print("accuracy :", acc)
                    print("recall :", rec)
                    print("precision :", pre)
                    print("f1 score : ", f1_v)
                    print("loss :", loss_val)
                    # ckpt_path = saver.save(sess,
                    #                        basic_path + "metrics_training_data/train_ckpt/" + self.project_name + "_" + str(self.faulty_version) + "/" + self.project_name + "_" + str(self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(step + 1) + "_nodes-" + self.node_number,
                    #                        step + 1)
                    # print("===== save ckpt file:", ckpt_path)
                    prediction = sess.run(tf.nn.sigmoid(model), feed_dict={X: test_x})
                    df = pd.DataFrame(prediction)
                    df.to_csv(self.base_path + self.train_result_data_ccm_path +"train_result/"+ self.project_name + "_" + str(
                        self.faulty_version) + "/max_" + self.project_name + "_" + str(
                        self.faulty_version) + "_b_train_result_sample-" + str(self.sample_number) + "_step-" + str(
                        step + 1) + "_nodes-" + self.node_number + ".csv", header=False)

            end = timeit.default_timer()
            print("Run Time", end - start)
            writer.flush()

if __name__ == "__main__":
    trainer = Trainer(project_name="Math", faulty_version="113", work_dir="closure_1_b", sample_num="2000", node_number="2000")
    # trainer.train_coverage_data()
    trainer.train_coverage_ccm_data()
