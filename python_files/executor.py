import os, subprocess
from os.path import isfile, join


class Executor:
    def __init__(self, ):
        self.d4j_env = os.environ["D4J_HOME"]
        self.pwd = os.getcwd()
        #self.d4j_env = "/mnt/d/workdir/defects4j"
        #self.pwd = "/mnt/c/Data/Chart"

        self.project_name = "Closure"
        self.project_name = "Lang"
        self.project_name_lower = self.project_name.lower()
        self.data_category = "/method_data/"
        self.stmt_path = "/framework/bin/fluccs/method_stmt/"
        self.sample_number = None
        self.node_number = None
        self.fault_list =[]
        self.base_path = self.pwd + "/outputs/"+self.project_name + self.data_category + "line_data"
        self.train_cac_number = None

    def execute_gen_coverage_data(self):
        # for number in range(1, 66):
        for number in self.fault_list:
            number = str(number)
            cmd = "nohup python3 main.py gen_coverage_data -p " + self.project_name + " -v " + number + " -w " + self.project_name_lower + "_" + number + "_b > a_files/gen_coverage_" + self.project_name + "_" + number + ".out & "
            result = subprocess.check_output(cmd, shell=True)

    def execute_gen_train_data(self, sample_number=500):
        self.sample_number = sample_number
        base_path = self.pwd + "/output/" + self.project_name + self.data_category
        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path)]
        for number in file_numbers:
            cmd = "nohup python3 main.py gen_train_data -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_gen_train_cac_data(self, sample_number=500):
        self.sample_number = sample_number
        file_numbers = self.get_line_info_number()
        print(file_numbers)

        # base_path = self.pwd + "/output/" + self.project_name + self.data_category
        # file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path)]
        for number in file_numbers:
            cmd = "nohup python3 main.py gen_train_cac_data -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_gen_train_cac_data_min(self, sample_number=500):
        self.sample_number = sample_number
        file_numbers = self.get_line_info_number()
        print(file_numbers)
        for number in file_numbers:
            cmd = "nohup python3 main.py gen_train_cac_data_min -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_gen_train_cac_data_max(self, sample_number=500):
        self.sample_number = sample_number
        file_numbers = self.get_line_info_number()
        print(file_numbers)
        for number in file_numbers:
            cmd = "nohup python3 main.py gen_train_cac_data_max -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_train_data(self, sample_number=500, node_number=1000):
        self.sample_number = sample_number
        self.node_number = node_number

        base_path = self.pwd + "/output/" + self.project_name + self.data_category
        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path)]
        for number in file_numbers:
            cmd = "nohup python3 main.py train_data -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " -n " + str(self.node_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_train_cac_data(self, sample_number=500, node_number=1000):
        self.sample_number = sample_number
        self.node_number = node_number

        base_path = self.pwd + "/output/" + self.project_name + self.data_category
        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path)]
        for number in file_numbers:
            cmd = "nohup python3 main.py train_cac_data -p " + self.project_name + " -v " + str(number) + " -s " + str(
                self.sample_number) + " -n " + str(self.node_number) + " &"
            result = subprocess.check_output(cmd, shell=True)

    def main_flow(self):
        pass

    def execute_tar_data_coverage(self):
        for number in range(1, 134):
            number = str(number)
            cmd = "nohup tar -cvjf coverage_output/" + self.project_name_lower + "_c_" + number + "_b_coverage.tar " + self.project_name_lower + "_" + number + "_b/output/ &"
            result = subprocess.check_output(cmd, shell=True)

    def execute_tar_data_testcases(self):
        for number in range(1, 134):
            number = str(number)
            cmd = "nohup tar -cvjf testcases_output/" + self.project_name_lower + "_c_" + number + "_b_testcases.tar " + self.project_name_lower + "_" + number + "_b/test_cases/ &"
            result = subprocess.check_output(cmd, shell=True)

    def check_folder(self):
        basic_path = "workD/checkout_data"
        basic_path = self.pwd
        for num in range(1, 134):
            folder_path = basic_path + "/" + self.project_name_lower+"_"+str(num)+"_b" + "/test_cases/coverage"
            if os.path.exists(basic_path + "/" + self.project_name_lower+"_"+str(num)+"_b" + "/test_cases/coverage"):
                file_names = [f for f in os.listdir(folder_path)]
                if file_names:
                    print("faulty ",self.project_name_lower,":", num)
                    self.fault_list.append(num)
        print(self.fault_list)


    def get_line_info_number(self):

        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path)]
        # print(file_numbers)
        sort_list = file_numbers.sort()

        int_results = list(map(int, file_numbers))
        sort_int_result= sorted(int_results)
        print(len(sort_int_result))
        print(sort_int_result)

        return file_numbers


    def get_train_cac_number(self, sample_number):
        self.base_path = self.pwd + "/outputs/"+self.project_name + "/method/line_data/"

        file_numbers = [f.split("_")[2] for f in os.listdir(self.base_path) if sample_number in f]
        print(file_numbers)
        #
        # int_results = list(map(int, file_numbers))
        # sort_int_result= sorted(int_results)
        # print(len(sort_int_result))
        # print(sort_int_result)
        self.train_cac_number = file_numbers
        return file_numbers

if __name__ == '__main__':
    e1 = Executor()
    e1.check_folder()
    # e1.execute_gen_coverage_data()
    e1.execute_gen_train_cac_data_min(500)
    e1.execute_gen_train_cac_data_min(1000)
    e1.execute_gen_train_cac_data_max(500)
    e1.execute_gen_train_cac_data_max(1000)
