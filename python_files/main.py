import sys, os

from configurator import Configurator
from drawer import Drawer
from concatenator import Concatenator
from generator import Generator
from trainer import Trainer


class Main:
    def __init__(self):
        pass

    def main_flow(self):
        configurator = Configurator()
        argv = configurator.set_argv(sys.argv[1:])

        if argv[0] == "gen_coverage_data":
            generator = Generator(project_name=argv[1], fault_version=argv[2], work_dir=argv[3], sample_num=None)
            generator.generate_coverage_data(generator.read_statement_info(), generator.read_coverage_files())

        elif argv[0] == "gen_coverage_ccm_data":
            generator = Generator(project_name=argv[1], fault_version=None, work_dir=None, sample_num=None)
            generator.generate_coverage_ccm_data()
            generator.generate_test_x_data()

        elif argv[0] == "gen_coverage_ccm_data_min":
            generator = Generator(project_name=argv[1], fault_version=None, work_dir=None, sample_num=None)
            generator.generate_coverage_ccm_data_min()
            generator.generate_test_x_data_min()

        elif argv[0] == "gen_coverage_ccm_data_max":
            generator = Generator(project_name=argv[1], fault_version=None, work_dir=None, sample_num=None)
            generator.generate_coverage_ccm_data_max()
            generator.generate_test_x_data_max()

        elif argv[0] == "gen_train_data":
            generator = Generator(project_name=argv[1], fault_version=argv[2], work_dir=None, sample_num=argv[3])
            generator.generate_train_data()

        elif argv[0] == "gen_train_ccm_data":
            generator = Generator(project_name=argv[1], fault_version=argv[2], work_dir=None, sample_num=argv[3])
            generator.generate_train_ccm_data()

        elif argv[0] == "gen_train_ccm_data_min":
            generator = Generator(project_name=argv[1], fault_version=argv[2], work_dir=None, sample_num=argv[3])
            generator.generate_train_ccm_data_min()

        elif argv[0] == "gen_train_ccm_data_max":
            generator = Generator(project_name=argv[1], fault_version=argv[2], work_dir=None, sample_num=argv[3])
            generator.generate_train_ccm_data_max()

        elif argv[0] == "train_data":
            trainer = Trainer(project_name=argv[1], faulty_version=argv[2], work_dir=None, sample_num=argv[3], node_number=argv[4])
            trainer.train_coverage_data()

        elif argv[0] == "train_ccm_data":
            trainer = Trainer(project_name=argv[1], faulty_version=argv[2], work_dir=None, sample_num=argv[3], node_number=argv[4])
            trainer.train_coverage_ccm_data()

        elif argv[0] == "train_ccm_data_min":
            trainer = Trainer(project_name=argv[1], faulty_version=argv[2], work_dir=None, sample_num=argv[3], node_number=argv[4])
            trainer.train_coverage_ccm_data_min()

        elif argv[0] == "train_ccm_data_max":
            trainer = Trainer(project_name=argv[1], faulty_version=argv[2], work_dir=None, sample_num=argv[3], node_number=argv[4])
            trainer.train_coverage_ccm_data_max()

        elif argv[0] == "gen_fault_data":
            generator = Generator(project_name=argv[1], fault_version=None, work_dir=None, sample_num=None)
            generator.get_method_fault_data()

        elif argv[0] == "gen_result_data":
            concatenator = Concatenator(project_name=argv[1], sample_num=argv[2], step_num=argv[3], node_number=argv[4])

            print("=== Start === Merge faulty data")
            concatenator.generate_rank_data_v1()

            # print("=== Start === Merge faulty ccm data")
            concatenator.generate_rank_data_metrics()

            # print("=== Start === Merge faulty ccm data min")
            concatenator.generate_rank_data_metrics_min()

            # print("=== Start === Merge faulty ccm data max")
            concatenator.generate_rank_data_metrics_max()
            print("=== End === Merge faulty data\n")

            print("=== Start === Draw graph total data")
            drawer = Drawer(project_name=argv[1], sample_num=argv[2], step_num=argv[3], node_number=argv[4])
            drawer.draw_result_metrics_total_v2()
            print("=== End === Draw graph total data")

        elif argv[0] == "gen_result_ccm_data":
            # print("=== Start === Merge faulty data")
            concatenator = Concatenator(project_name=argv[1], sample_num=argv[2], step_num=argv[3], node_number=argv[4])
            concatenator.generate_rank_data_metrics()
            # print("=== End === Merge faulty data")

            # print("=== Start === Draw graph data")
            drawer = Drawer(project_name=argv[1], sample_num=argv[2], step_num=argv[3], node_number=argv[4])
            drawer.draw_result_metrics()
            # print("=== End === Draw graph data")


if __name__ == '__main__':
    main = Main()
    main.main_flow()
